//
//  GameViewController.m
//  Pong
//
//  Created by Daniel Hansson on 2016-02-17.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()

@property (nonatomic) NSTimer *timer;
@property (nonatomic ) UIView *player;
@property (nonatomic) UIView *computer;
@property (nonatomic) UIView *ball;
@property (nonatomic) int X;
@property (nonatomic) int Y;
@property (nonatomic) int startLevel;
@property (nonatomic) int playerPoint;
@property (nonatomic) int computerPoint;
@property (nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic) UILabel *playerResult;
@property (nonatomic) UILabel *computerResult;
@property (weak, nonatomic) IBOutlet UIButton *restartButton;

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    self.playerResult = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x+30, 30, 70, 70)];
    [self.view addSubview:self.playerResult];
    self.playerResult.text = @"-";
    self.playerResult.textAlignment = NSTextAlignmentCenter;
    [self.playerResult setFont:[UIFont boldSystemFontOfSize:50]];
    self.playerResult.textColor = [UIColor whiteColor];
    self.playerPoint = 0;
    
    self.computerResult = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x-100, 30, 70, 70)];
    [self.view addSubview:self.computerResult];
    self.computerResult.text = @"-";
    self.computerResult.textAlignment = NSTextAlignmentCenter;
    [self.computerResult setFont:[UIFont boldSystemFontOfSize:50]];
    self.computerResult.textColor = [UIColor whiteColor];
    
    self.player = [[UIView alloc] initWithFrame:CGRectMake (self.view.frame.size.width -65, self.view.frame.size.height/2-30, 10.0, 60.0)];
    [self.view addSubview:self.player];
    self.player.backgroundColor = [UIColor whiteColor];
    
    self.computer = [[UIView alloc] initWithFrame:CGRectMake (55.0, self.view.frame.size.height/2, 10.0, 60.0)];
    [self.view addSubview:self.computer];
    self.computer.backgroundColor = [UIColor whiteColor];
    
    self.ball = [[UIView alloc] initWithFrame:CGRectMake (self.view.center.x, self.view.center.y, 16.0, 16.0)];
    [self.view addSubview:self.ball];
    self.ball.backgroundColor = [UIColor whiteColor];
    
    self.startLevel = 1;
    // Pan recognizer.
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePlayer:)];
    [self.view addGestureRecognizer:self.panRecognizer];
    
    // Sets up the sounds.
    NSURL *wallSoundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"wall" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)wallSoundURL, &playWallSound);
    
    NSURL *playerSoundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"player" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)playerSoundURL, &playPlayerSound);
    
    NSURL *computerSoundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"computer" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)computerSoundURL, &playComputerSound);
    
    //self.restartButton.center = CGPointMake(self.view.center.x, self.view.center.y);
    self.restartButton.hidden = YES;
    [self startGame];
}
- (IBAction)restartGameButton:(id)sender {
    self.playerPoint = 0;
    self.computerPoint = 0;
    self.restartButton.hidden = YES;
    self.startLevel = 0;
    [self startGame];
}

-(void)startGame {
    self.playerResult.text = [NSString stringWithFormat:@"%d",self.playerPoint];
    self.computerResult.text = [NSString stringWithFormat:@"%d",self.computerPoint];
    if (self.playerPoint<5 && self.computerPoint<5) {
        //self.Y = -self.startLevel + arc4random() % (self.startLevel*2)+1;
        //self.X = -self.startLevel + arc4random() % (self.startLevel*2)+1;
        self.Y = -2 + arc4random() % 3;
        self.X = -2 + arc4random() % 3;
        
        if (self.X == 0) {
            self.X = 1;
        }
        if (self.X<0) {
            self.X -= self.startLevel;
        }else{
            self.X += self.startLevel;
        }
        

        [self startTimer];
    }else {
        if (self.playerPoint == 5) {
            [self.restartButton setTitle:@"YOU WON! Click to play again." forState:normal];
        }else {
            [self.restartButton setTitle:@"YOU LOST! click to play again." forState:normal];
        }
        self.restartButton.hidden = NO;
    }
    

}

-(void)startTimer{
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:0.02
                  target:self
                  selector:@selector(moveBall)
                  userInfo:nil repeats:YES];

}

- (void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}
-(void)resetBall{
    self.ball.center = CGPointMake(self.view.center.x, self.view.center.y);
}

-(void) moveBall{
    self.ball.center = CGPointMake(self.ball.center.x + self.X, self.ball.center.y + self.Y);
    
    if (self.ball.center.y < 8) {
        AudioServicesPlaySystemSound(playWallSound);
        self.Y = 0 - self.Y;
    }
    if (self.ball.center.y > self.view.frame.size.height-8) {
        AudioServicesPlaySystemSound(playWallSound);
        self.Y = 0 - self.Y;
    }
    
    if (self.ball.center.x < 8) {
        [self stopTimer];
        self.playerPoint++;
        [self resetBall];
        self.startLevel+=1;
        [self startGame];
    }
    
    if (self.ball.center.x > self.view.frame.size.width-8) {
        [self stopTimer];
        self.computerPoint++;
        [self resetBall];
        self.startLevel+=1;
        [self startGame];
    }
    // Ball on player
    if (CGRectContainsPoint(self.player.frame, self.ball.center)) {
        AudioServicesPlaySystemSound(playPlayerSound);
        int angle = (self.player.center.y - self.ball.center.y)/10;
        self.Y -= angle;
        
        self.ball.center = CGPointMake(self.player.center.x -14, self.ball.center.y);
        self.X = 0 - self.X;
    }
    // Ball on computer
    if (CGRectContainsPoint(self.computer.frame, self.ball.center)) {
        AudioServicesPlaySystemSound(playComputerSound);
        int angle = (self.computer.center.y - self.ball.center.y)/10;
        self.Y -= angle;
        
        self.ball.center = CGPointMake(self.computer.center.x +14, self.ball.center.y);
        self.X = 0 - self.X;
        if (self.X<10) {
            self.X++;
        }
    }
    NSLog(@"%d",self.X);
    [self moveComputer];
}

-(void) moveComputer{
    
    if (self.computer.center.y < self.ball.center.y) {
        self.computer.center = CGPointMake(self.computer.center.x, self.computer.center.y + 3);
    }
    if (self.computer.center.y > self.ball.center.y) {
        self.computer.center = CGPointMake(self.computer.center.x, self.computer.center.y - 3);
    }
    if (self.computer.frame.origin.y < 1) {
        self.computer.center = CGPointMake(self.computer.center.x, 30);
    }
    if (self.computer.frame.origin.y+60 > self.view.frame.size.height) {
        self.computer.center = CGPointMake(self.computer.center.x, self.view.frame.size.height-30);
    }
    
}

- (void)movePlayer:(UIPanGestureRecognizer*)sender {
    
    if (self.player.center.y < [sender locationInView:self.view].y) {
        self.player.center = CGPointMake(self.player.center.x, self.player.center.y + 7);
    }
    if (self.player.center.y > [sender locationInView:self.view].y) {
        self.player.center = CGPointMake(self.player.center.x, self.player.center.y - 7);
    }
    if (self.player.frame.origin.y < 1) {
        self.player.center = CGPointMake(self.player.center.x, 30);
    }
    if (self.player.frame.origin.y+60 > self.view.frame.size.height) {
        self.player.center = CGPointMake(self.player.center.x, self.view.frame.size.height-30);
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

