//
//  AppDelegate.h
//  Pong
//
//  Created by Daniel Hansson on 2016-02-17.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

