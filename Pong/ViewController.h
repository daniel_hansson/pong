//
//  ViewController.h
//  Pong
//
//  Created by Daniel Hansson on 2016-02-17.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
{
    SystemSoundID playWallSound;
}

@end

