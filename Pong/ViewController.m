//
//  ViewController.m
//  Pong
//
//  Created by Daniel Hansson on 2016-02-17.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) NSTimer *timer;
@property (nonatomic) UIView *ball;
@property (weak, nonatomic) IBOutlet UIButton *startButton;


@end

@implementation ViewController
int X = 3;
int Y = 3;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blackColor];
    self.startButton.center = CGPointMake(self.view.center.x, self.view.center.y);
    
    self.ball = [[UIView alloc] initWithFrame:CGRectMake(276.0, 151.0, 16.0, 16.0)];
    [self.view addSubview:self.ball];
    self.ball.backgroundColor = [UIColor whiteColor];
    
    
    
    [self startDemo];
}

-(void)startDemo {
    
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:0.02
                  target:self
                  selector:@selector(moveBall)
                  userInfo:nil repeats:YES];
}

-(void) moveBall{
    self.ball.center = CGPointMake(self.ball.center.x + X, self.ball.center.y + Y);
    
    if (self.ball.center.y < 8) {
        Y = 0 - Y;
    }
    if (self.ball.center.y > self.view.frame.size.height-8) {
        Y = 0 - Y;
    }
    if (self.ball.center.x < 8) {
        X = 0 - X;
    }
    if (self.ball.center.x > self.view.frame.size.width-8) {
        X = 0 - X;
    }
    
    
}

- (void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
